* Json data files should be placed in data directory
* Credentials json data should be placed in credentials directory
* Please remember to set GOOGLE_AUTH_FILE_NAME env variable

Installation:

* provide envs
* composer install
* php bin/console doctrine:migrations:migrate
* php bin/console app:export-sheet sheetId (if you not provide sheetId wizard will ask for it)
