<?php declare(strict_types=1);

namespace App\Exporter;
use App\Client\GoogleClient;
use App\Model\ExporterInterface;
use Google_Service_Sheets;
use Google_Service_Sheets_ValueRange;
use Psr\Log\LoggerInterface;

/**
 * Class GoogleSheetExporter
 * @package App\Exporter
 */
class GoogleSheetExporter implements ExporterInterface
{

    /**
     * @var GoogleClient
     */
    private $googleClient;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * GoogleSheetExporter constructor.
     * @param GoogleClient $googleClient
     * @param LoggerInterface $logger
     */
    public function __construct(GoogleClient $googleClient, LoggerInterface $logger)
    {
        $this->googleClient = $googleClient;
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function exportSheet(string $sheetId, array $values = []): bool
    {
        $service = new Google_Service_Sheets($this->googleClient);

        $options = array('valueInputOption' => 'RAW');
        $body   = new Google_Service_Sheets_ValueRange(['values' => $values]);
        try {
            $result = $service->spreadsheets_values->update($sheetId, 'Sheet1', $body, $options);
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
            return false;
        }


        return true;
    }
}
