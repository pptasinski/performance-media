<?php declare(strict_types=1);

namespace App\Reader;
use App\Model\ReaderInterface;
use Symfony\Component\Finder\Finder;

/**
 * Class JsonReader
 * @package App\Reader
 */
final class JsonReader implements ReaderInterface
{

    /**
     * @var string
     */
    private $dataPath;

    /**
     * JsonReader constructor.
     * @param string $dataPath
     */
    public function __construct(string $dataPath)
    {
        $this->dataPath = $dataPath;
    }

    /**
     * @return array
     */
    public function readDataFileInToArray() : array
    {
        $finder = new Finder();
        $finder->files()->name('*.json')->in($this->dataPath);

        $iterator = $finder->getIterator();
        $iterator->rewind();
        $firstFile = $iterator->current();
        $data = array_values(json_decode($firstFile->getContents(), true));


        $parsedData = array_map(static function(array $array) {  $array['sub'] = is_array($array['sub']) ? implode(',', array_map(function ($subArray){ return $subArray['id'];}, $array['sub'])):''; return   array_values($array); } , $data);


        return array_merge([['Id', 'Title', 'Description', 'Summary', 'gtin', 'mpn', 'price', 'shortcode', 'category', 'sub', 'date'],], $parsedData);
    }

}
