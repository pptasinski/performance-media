<?php declare(strict_types=1);

namespace App\Manager;

use App\Entity\DataLog;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

/**
 * Class DataLogManager
 * @package App\Manager
 */
final class DataLogManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * DataLogManager constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $sheetId
     * @param array $data
     * @return DataLog
     */
    public function createLog(string $sheetId, array $data): DataLog
    {
        $log = new DataLog($sheetId, $data);
        $this->entityManager->persist($log);
        $this->entityManager->flush();
        return $log;
    }

    /**
     * @param DataLog $log
     * @throws Exception
     */
    public function finishDataImport(DataLog $log): void
    {
        $log->setImportState(DataLog::STATE_FINISHED, count($log->getData()));
        $this->entityManager->flush();
    }

    /**
     * @param DataLog $log
     * @throws Exception
     */
    public function endWithErrors(DataLog $log): void
    {
        $log->setImportState(DataLog::STATE_ERROR, 0);
        $this->entityManager->flush();
    }
}
