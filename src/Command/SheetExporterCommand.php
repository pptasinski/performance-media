<?php declare(strict_types=1);

namespace App\Command;

use App\Entity\DataLog;
use App\Exception\InvalidSheetIdException;
use App\Model\ExporterWorkerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * Class SheetExporterCommand
 * @package App\Command
 */
final class SheetExporterCommand extends Command
{

    /**
     * @var ExporterWorkerInterface
     */
    private $exporterWorker;


    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * SheetExporterCommand constructor.
     * @param ExporterWorkerInterface $exporterWorker
     */
    public function __construct(ExporterWorkerInterface $exporterWorker)
    {
        $this->exporterWorker = $exporterWorker;
        parent::__construct('app:export-sheet');

    }

    /**
     * {@inheritDoc}
     */
    protected function configure(): void
    {
        $this->setDescription('Exports google sheet')->setHelp('If you want to export google sheet write: app:export-sheet sheetId')->addArgument('sheetId', InputArgument::OPTIONAL, 'Target SheetId');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        parent::initialize($input, $output);
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function interact(InputInterface $input, OutputInterface $output): void
    {
        $sheetId = $input->getArgument('sheetId');
        if (null !== $sheetId) {
            return;
        }
        $this->io->title('Export sheet Command Interactive Wizard');
        $this->io->text(['If you prefer to not use this interactive wizard, provide the', 'arguments required by this command as follows:', '', ' $ php bin/console app:export-sheet sheetId', '', 'Now we\'ll ask you for the value of all the missing command arguments.',]);
        if (null !== $sheetId) {
            $this->io->text(' > <info>Sheet ID</info>: ' . $sheetId);
        } else {
            $sheetId = $this->io->ask('Sheet ID', null, [$this, 'validateSheetId']);
            $input->setArgument('sheetId', $sheetId);
        }

    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $stopwatch = new Stopwatch();
        $stopwatch->start('export-sheet-command');
        $sheetId = $input->getArgument('sheetId');
        $log = $this->exporterWorker->exportSheet($sheetId);
        $event = $stopwatch->stop('export-sheet-command');
        $finishedAt = $log->getFinishedAt() ? $log->getFinishedAt() : new \DateTime();
        $this->io->comment(sprintf('SheetID %s / Operation Id: %s / Finished at: %s / Elapsed time: %.2f ms / Consumed memory: %.2f MB / Status: %s / Rows: %s', $log->getSheetId(), $log->getId(), $finishedAt->format('Y-m-d H:i:s'), $event->getDuration(), $event->getMemory() / (1024 ** 2), $log->getImportState(), $log->getParsedRows()));
        return DataLog::STATE_FINISHED === $log->getImportState() ? 0 : 1;
    }

    /**
     * @param string|null $sheetId
     * @return string|null
     */
    public function validateSheetId(?string $sheetId): ?string
    {

        if (empty($sheetId)) {
            throw new InvalidSheetIdException('Invalid sheet id!');
        }
        return $sheetId;
    }

}
