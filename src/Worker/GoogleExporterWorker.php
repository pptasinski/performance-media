<?php declare(strict_types=1);

namespace App\Worker;

use App\Entity\DataLog;
use App\Exporter\GoogleSheetExporter;
use App\Manager\DataLogManager;
use App\Model\ExporterWorkerInterface;
use App\Reader\JsonReader;

/**
 * Class GoogleExporterWorker
 * @package App\Worker
 */
final class GoogleExporterWorker implements ExporterWorkerInterface
{
    /**
     * @var GoogleSheetExporter
     */
    private $exporter;
    /**
     * @var DataLogManager
     */
    private $logManager;
    /**
     * @var JsonReader
     */
    private $jsonReader;

    /**
     * GoogleExporterWorker constructor.
     * @param DataLogManager $logManager
     * @param GoogleSheetExporter $exporter
     * @param JsonReader $jsonReader
     */
    public function __construct(DataLogManager $logManager, GoogleSheetExporter $exporter, JsonReader $jsonReader)
    {
        $this->exporter = $exporter;
        $this->logManager = $logManager;
        $this->jsonReader = $jsonReader;
    }




    /**
     * @inheritDoc
     */
    public function exportSheet(string $sheetId): DataLog
    {

        $data  = $this->jsonReader->readDataFileInToArray();
        $log = $this->logManager->createLog($sheetId, $data);

        if(true === $this->exporter->exportSheet($sheetId, $data)){
            $this->logManager->finishDataImport($log);
        } else {
            $this->logManager->endWithErrors($log);
        }

        return $log;
    }
}
