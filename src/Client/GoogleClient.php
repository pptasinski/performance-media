<?php declare(strict_types=1);

namespace App\Client;

use Google_Exception;

/**
 * Class GoogleClient
 * @package App\Client
 */
class GoogleClient extends \Google_Client
{
    /**
     * GoogleClient constructor.
     * @param string $credentialsPath
     * @param string $authFile
     * @throws Google_Exception
     */
    public function __construct(string $credentialsPath, string $authFile)
    {
        parent::__construct([]);
        $this->setAuthConfig(sprintf('%s%s%s', $credentialsPath, DIRECTORY_SEPARATOR, $authFile));
        $this->addScope(\Google_Service_Sheets::SPREADSHEETS);
    }

}
