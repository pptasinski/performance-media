<?php declare(strict_types=1);

namespace App\Exception;
/**
 * Class InvalidSheetIdException
 * @package App\Exception
 */
final class InvalidSheetIdException extends \InvalidArgumentException
{

}
