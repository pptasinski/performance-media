<?php declare(strict_types=1);

namespace App\Model;

/**
 * Interface ExporterInterface
 * @package App\Model
 */
interface ExporterInterface
{
    /**
     * @param string $sheetId
     * @param array $values
     * @return bool
     */
    public function exportSheet(string $sheetId, array $values = []): bool;
}
