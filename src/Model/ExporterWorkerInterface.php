<?php declare(strict_types=1);

namespace App\Model;
use App\Entity\DataLog;

/**
 * Interface ExporterWorkerInterface
 * @package App\Model
 */
interface ExporterWorkerInterface
{
    /**
     * @param string $sheetId
     * @return DataLog
     */
    public function exportSheet(string $sheetId) : DataLog;
}
