<?php declare(strict_types=1);

namespace App\Model;

/**
 * Interface ReaderInterface
 * @package App\Model
 */
interface ReaderInterface
{

    public function readDataFileInToArray() : array ;
}
