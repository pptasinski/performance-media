<?php declare(strict_types=1);

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Exception;

/**
 * Class DataLog
 * @package App\Entity
 * @ORM\Entity
 * @ORM\Table(name="data_log", indexes={@ORM\Index(columns={"created_at"})})
 */
class DataLog
{
    public const STATE_PENDING = 'pending';

    public const STATE_ERROR = 'error';

    public const STATE_FINISHED = 'finished';

    /**
     * @var int|null
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $finishedAt;

    /**
     * @var string
     * @ORM\Column(type="string")
     *
     */
    private $sheetId;

    /**
     * @var array
     * @ORM\Column(type="array")
     */
    private $data;

    /**
     * @var string
     * @ORM\Column(type="string", length=50)
     */
    private $importState = self::STATE_PENDING;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $parsedRows = 0;

    public function __construct(string $sheetId, array $data)
    {
        $this->createdAt = new DateTime();
        $this->data = $data;
        $this->sheetId = $sheetId;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function getImportState(): string
    {
        return $this->importState;
    }

    /**
     * @return int
     */
    public function getParsedRows(): int
    {
        return $this->parsedRows;
    }


    /**
     * @return string
     */
    public function getSheetId(): string
    {
        return $this->sheetId;
    }

    /**
     * @return DateTime|null
     */
    public function getFinishedAt(): ?DateTime
    {
        return $this->finishedAt;
    }

    /**
     * @param DateTime|null $finishedAt
     * @return DataLog
     */
    public function setFinishedAt(?DateTime $finishedAt): DataLog
    {
        $this->finishedAt = $finishedAt;
        return $this;
    }

    /**
     * @param string $importState
     * @param int $rows
     * @return DataLog
     * @throws Exception
     */
    public function setImportState(string $importState, int $rows): DataLog
    {
        $this->parsedRows = $rows;
        $this->importState = $importState;
        $this->finishedAt = new DateTime();
        return $this;
    }
}
